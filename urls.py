from django.conf.urls import include, url
from django.contrib import admin

from intranet import api_urls as intranet_api_urls, urls as intranet_urls

urlpatterns = [
    # Examples:
    # url(r'^$', '.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(intranet_api_urls)),
    url(r'^', include(intranet_urls)),
]
