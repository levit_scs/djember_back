from decimal import Decimal
from datetime import timedelta

from django.db import models
from django.conf import settings
from django.core.exceptions import ValidationError

from polymorphic import PolymorphicModel
from simple_history.models import HistoricalRecords
from levit_utils.db import get_next_id
from levit_utils.states import StatefulModelMixin
from intranet import settings as isettings
from genericm2m.models import RelatedObjectsDescriptor

from crm.models import ModelWithAttachments, Company, Contact, Event


class Order(ModelWithAttachments, StatefulModelMixin):
  draft_number = models.CharField(max_length=10)
  order_number = models.CharField(max_length=10, blank=True, null=True)
  customer = models.ForeignKey(Company)
  customer_contact = models.ForeignKey(Contact, blank=True, null=True, related_name='+')

  created = models.DateTimeField(auto_now_add=True)
  updated = models.DateTimeField(auto_now=True)
  expiry = models.DateField(blank=True, null=True)

  owner = models.ForeignKey(settings.AUTH_USER_MODEL)

  history = HistoricalRecords()

  related = RelatedObjectsDescriptor()

  state = models.CharField(max_length=200, default='draft')
  _states = (
    'draft',
    'opened',
    'signed',
    'partially_invoiced',
    'invoiced',
    'done',
    'canceled',
  )
  _transitions = (
    ('draft', {'from': '*', 'to': 'draft'}),
    ('open', {'from': ['draft', 'signed'], 'to': 'opened'}),
    ('sign', {'from': ['opened', 'partially_invoiced', 'invoiced'], 'to': 'signed'}),
    ('invoice_partial', {'from': ['signed', 'partially_invoiced', 'invoiced'], 'to': 'partially_invoiced'}),
    ('invoice', {'from': ['signed', 'partially_invoiced', 'done'], 'to': 'invoiced'}),
    ('close', {'from': 'invoiced', 'to': 'done'}),
    ('cancel', {'from': ['draft', 'opened', 'signed', 'partially_invoiced'], 'to': 'canceled'}),
  )
  _state_groups = (
    ('running', ('opened', 'signed', 'partially_invoiced')),
    ('to_be_paid', ('partially_invoiced', 'invoiced')),
    ('closed', ('done', 'canceled'))
  )
  _company_field = 'customer'
  _contact_field = 'customer_contact'

  def __str__(self):
    if self.state in ['draft', 'canceled']:
      return '{} Order #{} ({})'.format(self.state.capitalize(), self.draft_number, self.untaxed_amount)
    else:
      return 'Order #{} ({})'.format(self.order_number, self.untaxed_amount)

  def state_change_opened(self, frm, to):
    if not self.order_number:
      self.order_number = get_next_id('order_number_sale_order', isettings)
    return True

  @property
  def untaxed_amount(self):
    return '{:.2f}'.format(sum([x.line_total() for x in self.lines.all()]))

  def print_str(self):
    if self.state in ['canceled', 'draft']:
      return self.draft_number
    else:
      return self.order_number

  def save(self, *args, **kwargs):
    e = None
    if self.pk is None and not self.draft_number:
      self.draft_number = get_next_id('draft_number_sale_order', isettings)
      e = Event(company=self.customer, what='New order created', mood=None)
    super(Order, self).save(*args, **kwargs)
    if e is not None:
      e.save()
      e.related.connect(self)

  @property
  def items(self):
    return self.lines.instance_of(OrderItem)

  @property
  def notes(self):
    return self.lines.instance_of(OrderNotes)

  @property
  def expires(self):
    if self.expiry:
      return self.expiry
    return self.updated + timedelta(days=30)


class Sellable(PolymorphicModel):
  name = models.CharField(max_length=255)
  unit_price = models.DecimalField(decimal_places=2, max_digits=7)

  def __str__(self):
    return '{} ({:.2f})'.format(self.name, self.unit_price)


class Service(Sellable):

  @property
  def type(self):
    return 'service'


class Product(Sellable):
  current_stock = models.IntegerField(default=0)
  threshold_stock = models.IntegerField(default=0)

  @property
  def type(self):
    return 'product'


class Option(Sellable):
  linked_to = models.ForeignKey(Sellable, related_name='options')

  @property
  def type(self):
    return 'option'


class Provisioning(models.Model):
  supplier = models.ForeignKey(Company, related_name='supplies')
  item = models.ForeignKey(Product)
  delay_in_days = models.IntegerField(default=0)


class OrderLine(PolymorphicModel):
  order = models.ForeignKey(Order, related_name='lines')
  position = models.PositiveIntegerField(default=0)

  def line_total(self):
    return Decimal(0.0)

  class Meta:
    ordering = ('position',)


class OrderItem(OrderLine):
  name = models.CharField(max_length=255, blank=True, null=True)
  quantity = models.DecimalField(decimal_places=2, max_digits=4)
  unit_price = models.DecimalField(decimal_places=2, max_digits=7, blank=True, null=True)

  item = models.ForeignKey(Sellable, blank=True, null=True)

  history = HistoricalRecords()

  def line_total(self):
    return self.unit_price * self.quantity

  def clean(self):
    if self.unit_price is None and self.item is None:
      raise ValidationError('You must specify an item or a unitprice (or both)')
    if self.unit_price is None:
      self.unit_price = self.item.unit_price
    if (self.name is None or self.name == '') and self.item is None:
      raise ValidationError('You must specify an item or a name (or both)')
    if self.name is None or self.name == '':
      self.name = self.item.name

  def __str__(self):
    return '{} ({:.2f})'.format(self.name, self.line_total())


class OrderNotes(OrderLine):
  type = models.CharField(max_length=10, choices=[
    ('title', 'title'),
    ('text', 'text'),
  ], default='text')
  value = models.TextField()

  history = HistoricalRecords()
