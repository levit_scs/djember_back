# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.db.models.deletion
import levit_utils.states


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('crm', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='HistoricalOrder',
            fields=[
                ('id', models.IntegerField(db_index=True, verbose_name='ID', blank=True, auto_created=True)),
                ('draft_number', models.CharField(max_length=10)),
                ('order_number', models.CharField(max_length=10, blank=True, null=True)),
                ('created', models.DateTimeField(editable=False, blank=True)),
                ('updated', models.DateTimeField(editable=False, blank=True)),
                ('expiry', models.DateField(blank=True, null=True)),
                ('state', models.CharField(max_length=200, default='draft')),
                ('history_id', models.AutoField(serialize=False, primary_key=True)),
                ('history_date', models.DateTimeField()),
                ('history_type', models.CharField(max_length=1, choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')])),
                ('customer', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='crm.Company', null=True)),
                ('customer_contact', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='crm.Contact', null=True)),
                ('history_user', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, null=True)),
                ('owner', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'verbose_name': 'historical order',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
        ),
        migrations.CreateModel(
            name='HistoricalOrderItem',
            fields=[
                ('id', models.IntegerField(db_index=True, verbose_name='ID', blank=True, auto_created=True)),
                ('position', models.PositiveIntegerField(default=0)),
                ('name', models.CharField(max_length=255, blank=True, null=True)),
                ('quantity', models.DecimalField(decimal_places=2, max_digits=4)),
                ('unit_price', models.DecimalField(blank=True, decimal_places=2, max_digits=7, null=True)),
                ('history_id', models.AutoField(serialize=False, primary_key=True)),
                ('history_date', models.DateTimeField()),
                ('history_type', models.CharField(max_length=1, choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')])),
                ('history_user', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'verbose_name': 'historical order item',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
        ),
        migrations.CreateModel(
            name='HistoricalOrderNotes',
            fields=[
                ('id', models.IntegerField(db_index=True, verbose_name='ID', blank=True, auto_created=True)),
                ('position', models.PositiveIntegerField(default=0)),
                ('type', models.CharField(max_length=10, default='text', choices=[('title', 'title'), ('text', 'text')])),
                ('value', models.TextField()),
                ('history_id', models.AutoField(serialize=False, primary_key=True)),
                ('history_date', models.DateTimeField()),
                ('history_type', models.CharField(max_length=1, choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')])),
                ('history_user', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'verbose_name': 'historical order notes',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('draft_number', models.CharField(max_length=10)),
                ('order_number', models.CharField(max_length=10, blank=True, null=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('expiry', models.DateField(blank=True, null=True)),
                ('state', models.CharField(max_length=200, default='draft')),
                ('customer', models.ForeignKey(to='crm.Company')),
                ('customer_contact', models.ForeignKey(related_name='+', blank=True, to='crm.Contact', null=True)),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model, levit_utils.states.StatefulModelMixin),
        ),
        migrations.CreateModel(
            name='OrderLine',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('position', models.PositiveIntegerField(default=0)),
            ],
            options={
                'ordering': ('position',),
            },
        ),
        migrations.CreateModel(
            name='Provisioning',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('delay_in_days', models.IntegerField(default=0)),
                ('supplier', models.ForeignKey(related_name='supplies', to='crm.Company')),
            ],
        ),
        migrations.CreateModel(
            name='Sellable',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(max_length=255)),
                ('unit_price', models.DecimalField(decimal_places=2, max_digits=7)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Option',
            fields=[
                ('sellable_ptr', models.OneToOneField(serialize=False, primary_key=True, auto_created=True, to='sale.Sellable', parent_link=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('sale.sellable',),
        ),
        migrations.CreateModel(
            name='OrderItem',
            fields=[
                ('orderline_ptr', models.OneToOneField(serialize=False, primary_key=True, auto_created=True, to='sale.OrderLine', parent_link=True)),
                ('name', models.CharField(max_length=255, blank=True, null=True)),
                ('quantity', models.DecimalField(decimal_places=2, max_digits=4)),
                ('unit_price', models.DecimalField(blank=True, decimal_places=2, max_digits=7, null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('sale.orderline',),
        ),
        migrations.CreateModel(
            name='OrderNotes',
            fields=[
                ('orderline_ptr', models.OneToOneField(serialize=False, primary_key=True, auto_created=True, to='sale.OrderLine', parent_link=True)),
                ('type', models.CharField(max_length=10, default='text', choices=[('title', 'title'), ('text', 'text')])),
                ('value', models.TextField()),
            ],
            options={
                'abstract': False,
            },
            bases=('sale.orderline',),
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('sellable_ptr', models.OneToOneField(serialize=False, primary_key=True, auto_created=True, to='sale.Sellable', parent_link=True)),
                ('current_stock', models.IntegerField(default=0)),
                ('threshold_stock', models.IntegerField(default=0)),
            ],
            options={
                'abstract': False,
            },
            bases=('sale.sellable',),
        ),
        migrations.CreateModel(
            name='Service',
            fields=[
                ('sellable_ptr', models.OneToOneField(serialize=False, primary_key=True, auto_created=True, to='sale.Sellable', parent_link=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('sale.sellable',),
        ),
        migrations.AddField(
            model_name='sellable',
            name='polymorphic_ctype',
            field=models.ForeignKey(editable=False, related_name='polymorphic_sale.sellable_set+', to='contenttypes.ContentType', null=True),
        ),
        migrations.AddField(
            model_name='orderline',
            name='order',
            field=models.ForeignKey(related_name='lines', to='sale.Order'),
        ),
        migrations.AddField(
            model_name='orderline',
            name='polymorphic_ctype',
            field=models.ForeignKey(editable=False, related_name='polymorphic_sale.orderline_set+', to='contenttypes.ContentType', null=True),
        ),
        migrations.AddField(
            model_name='historicalordernotes',
            name='order',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='sale.Order', null=True),
        ),
        migrations.AddField(
            model_name='historicalordernotes',
            name='orderline_ptr',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='sale.OrderLine', null=True),
        ),
        migrations.AddField(
            model_name='historicalordernotes',
            name='polymorphic_ctype',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='contenttypes.ContentType', null=True),
        ),
        migrations.AddField(
            model_name='historicalorderitem',
            name='item',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='sale.Sellable', null=True),
        ),
        migrations.AddField(
            model_name='historicalorderitem',
            name='order',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='sale.Order', null=True),
        ),
        migrations.AddField(
            model_name='historicalorderitem',
            name='orderline_ptr',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='sale.OrderLine', null=True),
        ),
        migrations.AddField(
            model_name='historicalorderitem',
            name='polymorphic_ctype',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='contenttypes.ContentType', null=True),
        ),
        migrations.AddField(
            model_name='provisioning',
            name='item',
            field=models.ForeignKey(to='sale.Product'),
        ),
        migrations.AddField(
            model_name='orderitem',
            name='item',
            field=models.ForeignKey(blank=True, to='sale.Sellable', null=True),
        ),
        migrations.AddField(
            model_name='option',
            name='linked_to',
            field=models.ForeignKey(related_name='options', to='sale.Sellable'),
        ),
    ]
