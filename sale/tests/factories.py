from factory import fuzzy
from factory.django import DjangoModelFactory
from factory import SubFactory

from ..models import Sellable, Product, Service, Option


class SellableFactory(DjangoModelFactory):

  class Meta:
    model = Sellable

  name = fuzzy.FuzzyText('product')
  unit_price = fuzzy.FuzzyDecimal(1)


class ProductFactory(SellableFactory):

  class Meta:
    model = Product

  current_stock = fuzzy.FuzzyInteger(0)
  threshold_stock = fuzzy.FuzzyInteger(5, 25)


class ServiceFactory(SellableFactory):

  class Meta:
    model = Service


class OptionFactory(SellableFactory):

  class Meta:
    model = Option

  linked_to = SubFactory(ServiceFactory)
