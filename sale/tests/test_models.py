from rest_framework.test import APITestCase

from intranet.tests.base import BaseAPITestCase

from .factories import ProductFactory, ServiceFactory, OptionFactory, SellableFactory

from ..models import Product, Service, Option, Sellable
from ..serializers import ProductSerializer, ServiceSerializer, OptionSerializer, SellableSerializer


class ProductAPITest(BaseAPITestCase, APITestCase):

  api_base_name = 'product'
  model = Product
  model_factory_class = ProductFactory
  serializer_class = ProductSerializer


class ServiceAPITest(BaseAPITestCase, APITestCase):

  api_base_name = 'service'
  model = Service
  model_factory_class = ServiceFactory
  serializer_class = ProductSerializer


class OptionAPITest(BaseAPITestCase, APITestCase):

  api_base_name = 'option'
  model = Option
  model_factory_class = OptionFactory
  serializer_class = OptionSerializer


class SellableAPITest(BaseAPITestCase, APITestCase):

  api_base_name = 'sellable'
  model = Sellable
  model_factory_class = ProductFactory
  serializer_class = SellableSerializer
  api_is_read_only = True
