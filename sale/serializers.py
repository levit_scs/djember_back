from django.utils.module_loading import import_string

from rest_framework import serializers

from .models import Sellable, Service, Product, Option


class ServiceSerializer(serializers.ModelSerializer):

  class Meta:
    model = Service
    fields = (
      'id',
      'type',
      'name',
      'unit_price',
    )


class ProductSerializer(serializers.ModelSerializer):

  class Meta:
    model = Product
    fields = (
      'id',
      'type',
      'name',
      'unit_price',
      'current_stock',
      'threshold_stock',
    )


class OptionSerializer(serializers.ModelSerializer):

  class Meta:
    model = Option
    fields = (
      'id',
      'type',
      'name',
      'unit_price',
      'linked_to',
    )


class SellableSerializer(serializers.ModelSerializer):

  class Meta:
    model = Sellable

  def to_representation(self, obj):
    serializer = import_string('sale.serializers.{}Serializer'.format(
      obj.type.title()
    ))
    return serializer(obj, context=self.context).to_representation(obj)
