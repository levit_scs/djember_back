from rest_framework import viewsets

from .serializers import (SellableSerializer, ProductSerializer,
  ServiceSerializer, OptionSerializer)
from .models import Sellable, Product, Service, Option


class ProductViewSet(viewsets.ModelViewSet):

  queryset = Product.objects.all()
  serializer_class = ProductSerializer


class SellableViewSet(viewsets.ReadOnlyModelViewSet):

  queryset = Sellable.objects.all()
  serializer_class = SellableSerializer


class ServiceViewSet(viewsets.ModelViewSet):

  queryset = Service.objects.all()
  serializer_class = ServiceSerializer


class OptionViewSet(viewsets.ModelViewSet):

  queryset = Option.objects.all()
  serializer_class = OptionSerializer
