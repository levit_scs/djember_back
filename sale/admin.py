from django.contrib import admin

from .models import Service, Product, Option


admin.site.register(Service)
admin.site.register(Product)
admin.site.register(Option)
