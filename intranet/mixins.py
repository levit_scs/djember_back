from functools import partial

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.db.models import Manager
from django.core.exceptions import ObjectDoesNotExist

## Admin

class ExtraIfNewInline:
    
    def get_extra(self, request, obj=None, **kwargs):
        if obj and obj.pk is not None:
            return 0
        return 3


## Views

class LoginRequiredMixin:
    
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(request, *args, **kwargs)


## REST Framework

def _get_related(attr, obj):

    try:
        target = getattr(obj, attr[0])
        if len(attr) == 1:
            return target
        else:
            if isinstance(target, Manager):
                target = target.latest()
            return _get_related(attr[1:], target)
    except ObjectDoesNotExist:
        return None


class RelatedModelSerializer:
    
    def __getattr__(self, name):
        invalid = False
        if name[:4] != 'get_':
            invalid = True
        else:
            to_get = name[4:].split('__')
            if len(to_get) < 2:
                invalid = True
        if invalid:
            raise AttributeError('unknown value {}'.format(name))

        return partial(_get_related, to_get)

    