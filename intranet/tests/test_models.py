from .factories import UserFactory, SectionFactory
from .base import BaseAPITestCase

from django.contrib.auth import get_user_model

from rest_framework.test import APITestCase

from ..models import Section


class UserAPITest(BaseAPITestCase, APITestCase):

  api_base_url = '/api/v1/userinfos/'
  model = get_user_model()
  model_factory_class = UserFactory

  list_requires_login = True
  detail_requires_login = True

  api_is_read_only = True


class SectionAPITest(BaseAPITestCase, APITestCase):

  api_base_url = '/api/v1/sections/'
  model = Section
  model_factory_class = SectionFactory

  api_is_read_only = True
