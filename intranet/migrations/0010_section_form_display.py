# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import intranet.models
import django_pgjson.fields


class Migration(migrations.Migration):

    dependencies = [
        ('intranet', '0009_remove_section_list_display'),
    ]

    operations = [
        migrations.AddField(
            model_name='section',
            name='form_display',
            field=django_pgjson.fields.JsonField(blank=True, default=intranet.models.default_list_display, null=True),
        ),
    ]
