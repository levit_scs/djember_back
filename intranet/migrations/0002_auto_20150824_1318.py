# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('intranet', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='PostalCode',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('geoname_id', models.IntegerField(unique=True, blank=True, null=True)),
                ('code', models.CharField(max_length=15)),
                ('city', models.ForeignKey(related_name='postal_code', to='cities_light.City')),
            ],
        ),
    ]
