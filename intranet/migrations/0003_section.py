# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.postgres.fields.hstore
from django.contrib.postgres.operations import HStoreExtension
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
        ('intranet', '0002_auto_20150824_1318'),
    ]

    operations = [
        HStoreExtension(),
        migrations.CreateModel(
            name='Section',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200, unique=True)),
                ('slug', models.SlugField(max_length=200)),
                ('icon', models.CharField(max_length=255)),
                ('opts', django.contrib.postgres.fields.hstore.HStoreField()),
                ('lft', models.PositiveIntegerField(db_index=True, editable=False)),
                ('rght', models.PositiveIntegerField(db_index=True, editable=False)),
                ('tree_id', models.PositiveIntegerField(db_index=True, editable=False)),
                ('level', models.PositiveIntegerField(db_index=True, editable=False)),
                ('parent', mptt.fields.TreeForeignKey(related_name='children', blank=True, to='intranet.Section', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
