# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.postgres.fields
import intranet.models


class Migration(migrations.Migration):

    dependencies = [
        ('intranet', '0006_section_route'),
    ]

    operations = [
        migrations.AddField(
            model_name='section',
            name='list_display',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=100), default=intranet.models.default_list_display, size=None),
        ),
    ]
