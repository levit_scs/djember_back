# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django_pgjson.fields


class Migration(migrations.Migration):

    dependencies = [
        ('intranet', '0010_section_form_display'),
    ]

    operations = [
        migrations.AddField(
            model_name='section',
            name='validations',
            field=django_pgjson.fields.JsonField(default={}),
        ),
    ]
