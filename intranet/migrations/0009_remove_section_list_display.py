# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models

import django_pgjson, intranet


class Migration(migrations.Migration):

    dependencies = [
        ('intranet', '0008_auto_20151022_1342'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='section',
            name='list_display',
        ),
        migrations.AddField(
            model_name='section',
            name='list_display',
            field=django_pgjson.fields.JsonField(null=True, blank=True, default=intranet.models.default_list_display),
        ),
    ]
