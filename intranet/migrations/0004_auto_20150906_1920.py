# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('intranet', '0003_section'),
    ]

    operations = [
        migrations.AlterField(
            model_name='section',
            name='icon',
            field=models.CharField(blank=True, null=True, max_length=255),
        ),
    ]
