# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import intranet.models
import django.contrib.postgres.fields


class Migration(migrations.Migration):

    dependencies = [
        ('intranet', '0007_section_list_display'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='section',
            name='slug',
        ),
        migrations.AddField(
            model_name='section',
            name='model',
            field=models.SlugField(null=True, max_length=200, blank=True),
        ),
        migrations.AlterField(
            model_name='section',
            name='list_display',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=100), null=True, size=None, default=intranet.models.default_list_display, blank=True),
        ),
        migrations.AlterField(
            model_name='section',
            name='route',
            field=models.CharField(choices=[('list', 'CRUD'), ('section', 'Section'), ('index', 'Dashboard')], default='index', max_length=255),
        ),
    ]
