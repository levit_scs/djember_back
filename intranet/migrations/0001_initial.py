# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations
from levit_utils import Migration as LevitMigration
import cities_light.abstract_models
import autoslug.fields


class Migration(LevitMigration):

    migrated_app = 'cities_light'

    dependencies = [
        ('cities_light', '0003_auto_20141120_0342'),
    ]

    operations = [
        migrations.AlterField(
            model_name='city',
            name='search_names',
            field=cities_light.abstract_models.ToSearchTextField(db_index=True, blank=True, max_length=4000, default=''),
        ),
        migrations.AlterField(
            model_name='city',
            name='slug',
            field=autoslug.fields.AutoSlugField(editable=False, populate_from='name_ascii'),
        ),
        migrations.AlterField(
            model_name='country',
            name='slug',
            field=autoslug.fields.AutoSlugField(editable=False, populate_from='name_ascii'),
        ),
        migrations.AlterField(
            model_name='region',
            name='slug',
            field=autoslug.fields.AutoSlugField(editable=False, populate_from='name_ascii'),
        ),
    ]
