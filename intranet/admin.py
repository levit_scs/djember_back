from django.contrib import admin
from django_mptt_admin.admin import DjangoMpttAdmin

from .models import Section


@admin.register(Section)
class SectionAdmin(DjangoMpttAdmin):

    list_display = ['title', 'route', 'model']
