from django.contrib.auth import get_user_model
from django.db.models import Q

from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework.generics import RetrieveAPIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.pagination import PageNumberPagination
from rest_framework.filters import DjangoFilterBackend

import django_filters

from .serializers import (UserSerializer, SectionSerializer, PostalCodeSerializer,
  CitySerializer, CountrySerializer)
from .models import Section, PostalCode
from cities_light.models import City, Country


class MeView(RetrieveAPIView):

  serializer_class = UserSerializer
  queryset = get_user_model().objects

  def get_object(self):
    return self.get_queryset().get(pk=self.request.user.id)


class UserInfoViewSet(ReadOnlyModelViewSet):

  serializer_class = UserSerializer
  permission_classes = (IsAuthenticated, )
  queryset = get_user_model().objects.all()


class SectionViewSet(ReadOnlyModelViewSet):

  serializer_class = SectionSerializer
  permission_class = AllowAny
  queryset = Section.objects.all()


class PaginatedReadOnlyModelViewSet(ReadOnlyModelViewSet):

  permission_class = AllowAny
  pagination_class = PageNumberPagination


class PostalCodeFilter(django_filters.FilterSet):

  q = django_filters.MethodFilter(action='zip_or_cityName_filter')

  class Meta:
    model = PostalCode
    fields = ['q', 'city__country']

  def zip_or_cityName_filter(self, queryset, value):
    return queryset.filter(Q(code__istartswith=value) | Q(city__name__icontains=value))


class PostalCodeViewSet(PaginatedReadOnlyModelViewSet):

  serializer_class = PostalCodeSerializer
  queryset = PostalCode.objects.all().prefetch_related('city', 'city__country')
  filter_backends = (DjangoFilterBackend, )
  filter_class = PostalCodeFilter


class CityViewSet(PaginatedReadOnlyModelViewSet):

  serializer_class = CitySerializer
  queryset = City.objects.all()


class CountryViewSet(ReadOnlyModelViewSet):

  serializer_class = CountrySerializer
  permission_class = AllowAny
  queryset = Country.objects.all()
