from __future__ import unicode_literals

import os.path

from django.conf import settings


__all__ = ['SOURCES', 'POSTALCODE_SOURCES', 'DATA_DIR', 'DRAFT_NUMBER_SALE_ORDER_FORMAT', 'ORDER_NUMBER_SALE_ORDER_FORMAT']

POSTALCODE_SOURCES = getattr(settings, 'INTRANET_POSTALCODE_SOURCES',
  ['http://download.geonames.org/export/dump/alternateNames.zip'])

SOURCES = list(POSTALCODE_SOURCES)

DATA_DIR = getattr(settings, 'CITIES_LIGHT_DATA_DIR',
  os.path.normpath(os.path.join(
    os.path.dirname(os.path.realpath(__file__)), 'data')))


DRAFT_NUMBER_SALE_ORDER_FORMAT = getattr(settings, 'INTRANET_DRAFT_NUMBER_SALE_ORDER_FORMAT', "{year}{month}{id:02d}")
ORDER_NUMBER_SALE_ORDER_FORMAT = getattr(settings, 'INTRANET_ORDER_NUMBER_SALE_ORDER_FORMAT', "{year}{month}{id:02d}")
