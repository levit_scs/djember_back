from django.contrib.auth import get_user_model

from rest_framework import serializers

from .models import Section, PostalCode
from cities_light.models import City, Country


class UserSerializer(serializers.ModelSerializer):

  full_name = serializers.SerializerMethodField()

  class Meta:
    model = get_user_model()
    fields = (
      'id',
      'username',
      'full_name',
      'email'
    )

  def get_full_name(self, obj):
    if obj.get_full_name():
      return obj.get_full_name()
    return obj.username


class SectionSerializer(serializers.ModelSerializer):

  list_display = serializers.JSONField()
  form_display = serializers.JSONField()
  validations = serializers.JSONField()

  class Meta:
    model = Section
    fields = (
      'id',
      'title',
      'route',
      'model',
      'icon',
      'parent',
      'children',
      'opts',
      'list_display',
      'form_display',
      'validations',
    )


class PostalCodeSerializer(serializers.ModelSerializer):

  class Meta:
    model = PostalCode
    fields = (
      'id',
      'city',
      'code',
      '__str__',
    )


class CitySerializer(serializers.ModelSerializer):

  class Meta:
    model = City
    fields = (
      'id',
      'name',
      'latitude',
      'longitude',
      'country',
    )


class CountrySerializer(serializers.ModelSerializer):

  class Meta:
    model = Country
    fields = (
      'id',
      'name',
      'code2',
      'code3',
    )
