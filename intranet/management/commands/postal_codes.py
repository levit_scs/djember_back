from __future__ import unicode_literals

import os
import time
import os.path
import logging
import optparse

from django.core.management.base import BaseCommand
from django.db import connection, reset_queries

from cities_light.management.commands.cities_light import MemoryUsageWidget

from cities_light.vendor import progressbar
from cities_light.exceptions import *
from cities_light.signals import *
from cities_light.models import *
from ...settings import *
from cities_light.geonames import Geonames
from ...models import PostalCode


class Command(BaseCommand):
  args = '[--force-all]'.strip()
  help = '''
Download  alternateNames.zip if they were updated or if
--force-all option was used.
Import postal code dataa if they were downloaded.
    '''.strip()

  logger = logging.getLogger('cities_light')

  option_list = BaseCommand.option_list + (
    optparse.make_option('--force-all', action='store_true', default=False,
                         help='Download and import if files are up-to-date.'
    ),
  )

  def _travis(self):
    if not os.environ.get('TRAVIS', False):
      return

    now = time.time()
    last_output = getattr(self, '_travis_last_output', None)

    if last_output is None or now - last_output >= 530:
      print('Do not kill me !')
      self._travis_last_output = now

  def handle(self, *args, **options):
    if not os.path.exists(DATA_DIR):
      self.logger.info('Creating %s' % DATA_DIR)
      os.mkdir(DATA_DIR)

    install_file_path = os.path.join(DATA_DIR, 'install_datetime')

    self.noinsert = False
    self.widgets = [
      'RAM used: ',
      MemoryUsageWidget(),
      ' ',
      progressbar.ETA(),
      ' Done: ',
      progressbar.Percentage(),
      progressbar.Bar(),
      ]

    for url in SOURCES:
      destination_file_name = url.split('/')[-1]

      force = options.get('force_all', False)

      geonames = Geonames(url, force=force)
      downloaded = geonames.downloaded

      force_import = options.get('force_import_all', False)

      if not os.path.exists(install_file_path):
        self.logger.info('Forced import of %s because data do not seem'
                         ' to have installed successfuly yet, note that this is'
                         ' equivalent to --force-import-all.' %
                         destination_file_name)
        force_import = True

      if downloaded or force_import:
        self.logger.info('Importing %s' % destination_file_name)

        i = 0
        progress = progressbar.ProgressBar(maxval=geonames.num_lines(),
                                           widgets=self.widgets).start()

        for items in geonames.parse():
          self.postalcode_parse(items)

          reset_queries()

          i += 1
          progress.update(i)

          self._travis()

        progress.finish()

  def _get_country_id(self, code2):
    '''
    Simple lazy identity map for code2->country
    '''
    if not hasattr(self, '_country_codes'):
      self._country_codes = {}

    if code2 not in self._country_codes.keys():
      self._country_codes[code2] = Country.objects.get(code2=code2).pk

    return self._country_codes[code2]

  def _get_region_id(self, country_code2, region_id):
    '''
    Simple lazy identity map for (country_code2, region_id)->region
    '''
    if not hasattr(self, '_region_codes'):
      self._region_codes = {}

    country_id = self._get_country_id(country_code2)
    if country_id not in self._region_codes:
      self._region_codes[country_id] = {}

    if region_id not in self._region_codes[country_id]:
      self._region_codes[country_id][region_id] = Region.objects.get(
        country_id=country_id, geoname_code=region_id).pk

    return self._region_codes[country_id][region_id]

  def postalcode_parse(self, items):
    if not hasattr(self, 'translation_data'):
      self.country_ids = Country.objects.values_list('geoname_id',
                                                     flat=True)
      self.region_ids = Region.objects.values_list('geoname_id',
                                                   flat=True)
      self.city_ids = City.objects.values_list('geoname_id', flat=True)

      self.translation_data = {
        Country: {},
        Region: {},
        City: {},
        }

    connection.close()

    if items[2] != 'post':
      return

    # arg optimisation code kills me !!!
    items[1] = int(items[1])
    items[0] = int(items[0])

    if items[1] not in self.city_ids:
      return

    try:
      code = PostalCode.objects.get(geoname_id=items[0])
    except PostalCode.DoesNotExist:
      code = PostalCode()
      code.geoname_id = items[0]
    try:
      code.city = City.objects.get(geoname_id=items[1])
      postal_code = ' '.join(items[3:])
      if code.code != postal_code:
        code.code = postal_code
        code.save()
    except:
      pass
