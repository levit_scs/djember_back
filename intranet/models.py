from django.db import models
from django.contrib.postgres.fields import HStoreField
from django_pgjson.fields import JsonField
from mptt.models import MPTTModel, TreeForeignKey

from cities_light.models import City


class PostalCodeManager(models.Manager):

  def get_queryset(self):
    return super(PostalCodeManager, self).get_queryset().prefetch_related('city', 'city__country')


class PostalCode(models.Model):
  city = models.ForeignKey(City, related_name='postal_code')
  geoname_id = models.IntegerField(null=True, blank=True, unique=True)
  code = models.CharField(max_length=15)

  def __str__(self):
    return '{} {} {}'.format(self.code, self.city.name, self.city.country.code2)

  @property
  def name(self):
    return '{} {}'.format(self.code, self.city.name)

  def distance_from(self, other_zip):
    from math import radians, cos, sin, asin, sqrt
    lon1, lat1, lon2, lat2 = map(radians, [
      self.city.longitude,
      self.city.latitude,
      other_zip.city.longitude,
      other_zip.city.latitude
    ])
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * asin(sqrt(a))

    # 6367 km is the radius of the Earth
    km = 6367 * c
    return km


def default_list_display():
  return [{'field': '__str__', }, ]


ROUTE_TYPES = (
  ('list', 'CRUD'),
  ('section', 'Section'),
  ('index', 'Dashboard'),
)


class Section(MPTTModel):

  title = models.CharField(max_length=200, unique=True)
  model = models.SlugField(max_length=200, unique=False, blank=True, null=True)
  icon = models.CharField(max_length=255, blank=True, null=True)
  opts = HStoreField(blank=True, null=True)
  parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True)
  route = models.CharField(max_length=255, choices=ROUTE_TYPES, default='index')
  list_display = JsonField(
    default=default_list_display,
    blank=True,
    null=True
  )
  form_display = JsonField(
    default=default_list_display,
    blank=True,
    null=True
  )
  validations = JsonField(default={})

  def __str__(self):
    return self.title
