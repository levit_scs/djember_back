from django.conf.urls import include, url
from rest_framework import routers
from rest_framework.authtoken import views

from .views import (MeView, UserInfoViewSet, SectionViewSet, PostalCodeViewSet,
  CityViewSet, CountryViewSet)
from crm.views import CompanyViewSet, SectorViewSet, ContactMechanismViewSet
from sale.views import (SellableViewSet, ProductViewSet, ServiceViewSet,
  OptionViewSet)


router = routers.DefaultRouter()
router.register(r'userinfos', UserInfoViewSet)
router.register(r'sections', SectionViewSet)
router.register(r'companies', CompanyViewSet)
router.register(r'contactmechanisms', ContactMechanismViewSet)
router.register(r'sellables', SellableViewSet)
router.register(r'products', ProductViewSet)
router.register(r'services', ServiceViewSet)
router.register(r'options', OptionViewSet)
router.register(r'sectors', SectorViewSet)
router.register(r'postalcodes', PostalCodeViewSet)
router.register(r'cities', CityViewSet)
router.register(r'countries', CountryViewSet)

urlpatterns = [
    url(r'^v(?P<version>\d+)/me/', MeView.as_view()),
    url(r'^v(?P<version>\d+)/', include(router.urls)),
    url(r'^auth/', views.obtain_auth_token),
]
