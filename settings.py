"""
Django settings for Intranet NG project.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.8/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

try:
    from environ import *
except ImportError:
    raise Exception('Improperly configured: Please make an untracked copy of '
                    'environ.py.dist to environ.py and update with your '
                    'actual settings')

# Application definition

INSTALLED_APPS = (
    'intranet',

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.postgres',
    ## Django-polymorphic
    'polymorphic',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # REST Framework
    'rest_framework',
    'rest_framework.authtoken',
    # 'crispy_forms',
    # MPTT
    'mptt',
    'django_mptt_admin',

    ## Celery
    'kombu.transport.django.KombuAppConfig',

    ## Cities
    'cities_light',

    ## Versionning
    'simple_history',

    ## Generic-m2m
    'genericm2m',

    ## Reports
    'levit_report',

    ## Project
    'crm',
    'sale',
    'mailing',

    ## Tests
    'factory',
    'django_jenkins',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

ROOT_URLCONF = 'urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
            'loaders': (
                'django.template.loaders.app_directories.Loader',
            ),
        },
    },
]

WSGI_APPLICATION = 'intranet.wsgi.application'


# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

STATIC_URL = '/assets/'
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)
STATIC_ROOT = os.path.join(BASE_DIR, 'assets')

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly',
    ],
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
    ),
    'PAGE_SIZE': 1000,
    # 'DEFAULT_FILTER_BACKENDS': ('rest_framework.filters.DjangoFilterBackend',)
}

if DEBUG:
    REST_FRAMEWORK['DEFAULT_AUTHENTICATION_CLASSES'] = (
        'rest_framework.authentication.TokenAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    )
    INSTALLED_APPS += (
        'debug_toolbar',
        'debug_panel',
        'corsheaders',
    )

    MIDDLEWARE_CLASSES += (
        'debug_panel.middleware.DebugPanelMiddleware',
        'corsheaders.middleware.CorsMiddleware',
    )


CORS_ORIGIN_ALLOW_ALL = DEBUG

MIGRATION_MODULES = {
    'genericm2m': 'intranet.genericm2m_migrations',
}

# Cities
CITIES_POSTAL_CODES = ['BE','FR','NL','LU', 'US', ]
CITIES_LIGHT_CITY_SOURCES = [
    'http://download.geonames.org/export/dump/cities1000.zip',
    'http://download.geonames.org/export/dump/BE.zip',
    'http://download.geonames.org/export/dump/NL.zip',
    'http://download.geonames.org/export/dump/FR.zip',
    'http://download.geonames.org/export/dump/LU.zip',
]
CITIES_LIGHT_TRANSLATION_LANGUAGES = ['en', 'fr', ]
