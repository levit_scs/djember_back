# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0002_auto_20150905_0818'),
    ]

    operations = [
        migrations.RenameField(
            model_name='contactmechanism',
            old_name='type',
            new_name='machanism_type',
        ),
        migrations.RenameField(
            model_name='historicalcontactmechanism',
            old_name='type',
            new_name='machanism_type',
        ),
    ]
