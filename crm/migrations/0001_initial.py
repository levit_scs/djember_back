# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.utils.timezone
import django.db.models.deletion
import crm.models


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('intranet', '0002_auto_20150824_1318'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('street', models.TextField()),
                ('is_invoice', models.BooleanField(default=True)),
                ('is_active', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='Attachement',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(max_length=255)),
                ('file', models.FileField(upload_to='uploads')),
                ('object_id', models.PositiveIntegerField()),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
            ],
        ),
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('is_default_contact', models.BooleanField(default=False)),
                ('notes', models.TextField(blank=True, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ContactMechanism',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('type', models.CharField(max_length=50)),
                ('value', models.CharField(max_length=255)),
                ('include_in_mailing', models.BooleanField(default=False)),
                ('has_unsubscribed', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('when', models.DateTimeField(default=django.utils.timezone.now)),
                ('what', models.CharField(max_length=255)),
                ('mood', models.CharField(blank=True, max_length=8, default=':|', null=True)),
                ('summary', models.TextField(blank=True, null=True)),
            ],
            options={
                'ordering': ['-when'],
            },
        ),
        migrations.CreateModel(
            name='HistoricalAddress',
            fields=[
                ('id', models.IntegerField(db_index=True, verbose_name='ID', blank=True, auto_created=True)),
                ('street', models.TextField()),
                ('is_invoice', models.BooleanField(default=True)),
                ('is_active', models.BooleanField(default=True)),
                ('history_id', models.AutoField(serialize=False, primary_key=True)),
                ('history_date', models.DateTimeField()),
                ('history_type', models.CharField(max_length=1, choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')])),
            ],
            options={
                'verbose_name': 'historical address',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
        ),
        migrations.CreateModel(
            name='HistoricalAttachement',
            fields=[
                ('id', models.IntegerField(db_index=True, verbose_name='ID', blank=True, auto_created=True)),
                ('name', models.CharField(max_length=255)),
                ('file', models.TextField(max_length=100)),
                ('object_id', models.PositiveIntegerField()),
                ('history_id', models.AutoField(serialize=False, primary_key=True)),
                ('history_date', models.DateTimeField()),
                ('history_type', models.CharField(max_length=1, choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')])),
                ('content_type', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='contenttypes.ContentType', null=True)),
                ('history_user', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'verbose_name': 'historical attachement',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
        ),
        migrations.CreateModel(
            name='HistoricalCompany',
            fields=[
                ('id', models.IntegerField(db_index=True, verbose_name='ID', blank=True, auto_created=True)),
                ('is_default_contact', models.BooleanField(default=False)),
                ('notes', models.TextField(blank=True, null=True)),
                ('name', models.CharField(max_length=255)),
                ('is_customer', models.BooleanField(default=False)),
                ('is_partner', models.BooleanField(default=False)),
                ('is_supplier', models.BooleanField(default=False)),
                ('vat', models.CharField(max_length=30, blank=True, null=True)),
                ('history_id', models.AutoField(serialize=False, primary_key=True)),
                ('history_date', models.DateTimeField()),
                ('history_type', models.CharField(max_length=1, choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')])),
            ],
            options={
                'verbose_name': 'historical company',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
        ),
        migrations.CreateModel(
            name='HistoricalContactMechanism',
            fields=[
                ('id', models.IntegerField(db_index=True, verbose_name='ID', blank=True, auto_created=True)),
                ('type', models.CharField(max_length=50)),
                ('value', models.CharField(max_length=255)),
                ('include_in_mailing', models.BooleanField(default=False)),
                ('has_unsubscribed', models.BooleanField(default=False)),
                ('history_id', models.AutoField(serialize=False, primary_key=True)),
                ('history_date', models.DateTimeField()),
                ('history_type', models.CharField(max_length=1, choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')])),
            ],
            options={
                'verbose_name': 'historical contact mechanism',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
        ),
        migrations.CreateModel(
            name='HistoricalEvent',
            fields=[
                ('id', models.IntegerField(db_index=True, verbose_name='ID', blank=True, auto_created=True)),
                ('when', models.DateTimeField(default=django.utils.timezone.now)),
                ('what', models.CharField(max_length=255)),
                ('mood', models.CharField(blank=True, max_length=8, default=':|', null=True)),
                ('summary', models.TextField(blank=True, null=True)),
                ('history_id', models.AutoField(serialize=False, primary_key=True)),
                ('history_date', models.DateTimeField()),
                ('history_type', models.CharField(max_length=1, choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')])),
                ('history_user', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'verbose_name': 'historical event',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
        ),
        migrations.CreateModel(
            name='HistoricalPerson',
            fields=[
                ('id', models.IntegerField(db_index=True, verbose_name='ID', blank=True, auto_created=True)),
                ('is_default_contact', models.BooleanField(default=False)),
                ('notes', models.TextField(blank=True, null=True)),
                ('last_name', models.CharField(max_length=255, blank=True, null=True)),
                ('first_name', models.CharField(max_length=255)),
                ('gender', models.CharField(max_length=1, null=True, blank=True, choices=[('f', 'Female'), ('m', 'Male'), (None, 'unknown')])),
                ('history_id', models.AutoField(serialize=False, primary_key=True)),
                ('history_date', models.DateTimeField()),
                ('history_type', models.CharField(max_length=1, choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')])),
            ],
            options={
                'verbose_name': 'historical person',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
        ),
        migrations.CreateModel(
            name='HistoricalReminder',
            fields=[
                ('id', models.IntegerField(db_index=True, verbose_name='ID', blank=True, auto_created=True)),
                ('subject', models.CharField(max_length=255)),
                ('when', models.DateTimeField()),
                ('state', models.CharField(max_length=10, default='open', choices=[('open', 'running'), ('dismissed', 'accepted'), ('canceled', 'canceled')])),
                ('description', models.TextField(blank=True, null=True)),
                ('send_email', models.BooleanField(default=False)),
                ('send_text', models.BooleanField(default=False)),
                ('history_id', models.AutoField(serialize=False, primary_key=True)),
                ('history_date', models.DateTimeField()),
                ('history_type', models.CharField(max_length=1, choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')])),
            ],
            options={
                'verbose_name': 'historical reminder',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
        ),
        migrations.CreateModel(
            name='Reminder',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('subject', models.CharField(max_length=255)),
                ('when', models.DateTimeField()),
                ('state', models.CharField(max_length=10, default='open', choices=[('open', 'running'), ('dismissed', 'accepted'), ('canceled', 'canceled')])),
                ('description', models.TextField(blank=True, null=True)),
                ('send_email', models.BooleanField(default=False)),
                ('send_text', models.BooleanField(default=False)),
            ],
            options={
                'ordering': ('-state', 'when'),
            },
        ),
        migrations.CreateModel(
            name='Sector',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Segment',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Company',
            fields=[
                ('contact_ptr', models.OneToOneField(serialize=False, primary_key=True, auto_created=True, to='crm.Contact', parent_link=True)),
                ('name', models.CharField(max_length=255)),
                ('is_customer', models.BooleanField(default=False)),
                ('is_partner', models.BooleanField(default=False)),
                ('is_supplier', models.BooleanField(default=False)),
                ('vat', models.CharField(max_length=30, blank=True, null=True)),
                ('sector', models.ForeignKey(blank=True, to='crm.Sector', null=True)),
                ('segments', models.ManyToManyField(to='crm.Segment', blank=True, null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('crm.contact',),
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('contact_ptr', models.OneToOneField(serialize=False, primary_key=True, auto_created=True, to='crm.Contact', parent_link=True)),
                ('last_name', models.CharField(max_length=255, blank=True, null=True)),
                ('first_name', models.CharField(max_length=255)),
                ('gender', models.CharField(max_length=1, null=True, blank=True, choices=[('f', 'Female'), ('m', 'Male'), (None, 'unknown')])),
                ('belongs_to', models.ForeignKey(related_name='members', blank=True, to='crm.Company', null=True)),
                ('user', models.OneToOneField(related_name='person', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('crm.contact',),
        ),
        migrations.AddField(
            model_name='reminder',
            name='contact',
            field=models.ForeignKey(blank=True, to='crm.Contact', null=True),
        ),
        migrations.AddField(
            model_name='reminder',
            name='owner',
            field=models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='historicalreminder',
            name='contact',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='crm.Contact', null=True),
        ),
        migrations.AddField(
            model_name='historicalreminder',
            name='history_user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='historicalreminder',
            name='owner',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='historicalperson',
            name='contact_ptr',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='crm.Contact', null=True),
        ),
        migrations.AddField(
            model_name='historicalperson',
            name='history_user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='historicalperson',
            name='internal_contact',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='historicalperson',
            name='polymorphic_ctype',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='contenttypes.ContentType', null=True),
        ),
        migrations.AddField(
            model_name='historicalperson',
            name='user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='historicalcontactmechanism',
            name='contact',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='crm.Contact', null=True),
        ),
        migrations.AddField(
            model_name='historicalcontactmechanism',
            name='history_user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='historicalcompany',
            name='contact_ptr',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='crm.Contact', null=True),
        ),
        migrations.AddField(
            model_name='historicalcompany',
            name='history_user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='historicalcompany',
            name='internal_contact',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='historicalcompany',
            name='polymorphic_ctype',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='contenttypes.ContentType', null=True),
        ),
        migrations.AddField(
            model_name='historicalcompany',
            name='sector',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='crm.Sector', null=True),
        ),
        migrations.AddField(
            model_name='historicaladdress',
            name='contact',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='crm.Contact', null=True),
        ),
        migrations.AddField(
            model_name='historicaladdress',
            name='country',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='cities_light.Country', null=True),
        ),
        migrations.AddField(
            model_name='historicaladdress',
            name='history_user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='historicaladdress',
            name='region',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='cities_light.Region', null=True),
        ),
        migrations.AddField(
            model_name='historicaladdress',
            name='zip',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='intranet.PostalCode', null=True),
        ),
        migrations.AddField(
            model_name='contactmechanism',
            name='contact',
            field=models.ForeignKey(to='crm.Contact'),
        ),
        migrations.AddField(
            model_name='contact',
            name='internal_contact',
            field=models.ForeignKey(related_name='contacts', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='contact',
            name='polymorphic_ctype',
            field=models.ForeignKey(editable=False, related_name='polymorphic_crm.contact_set+', to='contenttypes.ContentType', null=True),
        ),
        migrations.AddField(
            model_name='address',
            name='contact',
            field=models.ForeignKey(related_name='addresses', to='crm.Contact'),
        ),
        migrations.AddField(
            model_name='address',
            name='country',
            field=models.ForeignKey(to='cities_light.Country', default=crm.models._belgium),
        ),
        migrations.AddField(
            model_name='address',
            name='region',
            field=models.ForeignKey(blank=True, to='cities_light.Region', null=True),
        ),
        migrations.AddField(
            model_name='address',
            name='zip',
            field=models.ForeignKey(related_name='addresses', blank=True, verbose_name='City', to='intranet.PostalCode', null=True),
        ),
        migrations.AddField(
            model_name='historicalperson',
            name='belongs_to',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='crm.Company', null=True),
        ),
        migrations.AddField(
            model_name='historicalevent',
            name='company',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='crm.Company', null=True),
        ),
        migrations.AddField(
            model_name='historicalevent',
            name='person',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='crm.Person', null=True),
        ),
        migrations.AddField(
            model_name='event',
            name='company',
            field=models.ForeignKey(blank=True, to='crm.Company', null=True),
        ),
        migrations.AddField(
            model_name='event',
            name='person',
            field=models.ForeignKey(blank=True, verbose_name='who', to='crm.Person', null=True),
        ),
    ]
