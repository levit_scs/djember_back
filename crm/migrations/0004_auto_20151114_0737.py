# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0003_auto_20151114_0734'),
    ]

    operations = [
        migrations.RenameField(
            model_name='contactmechanism',
            old_name='machanism_type',
            new_name='mechanism_type',
        ),
        migrations.RenameField(
            model_name='historicalcontactmechanism',
            old_name='machanism_type',
            new_name='mechanism_type',
        ),
    ]
