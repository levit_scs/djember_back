from rest_framework.test import APITestCase

from intranet.tests.base import BaseAPITestCase

from .factories import CompanyFactory, SectorFactory

from ..models import Company, Sector
from ..serializers import CompanySerializer, SectorSerializer


class CompanyAPITest(BaseAPITestCase, APITestCase):

  api_base_name = 'company'
  model = Company
  model_factory_class = CompanyFactory
  serializer_class = CompanySerializer


class SectorAPITest(BaseAPITestCase, APITestCase):

  api_base_name = 'sector'
  model = Sector
  model_factory_class = SectorFactory
  serializer_class = SectorSerializer
