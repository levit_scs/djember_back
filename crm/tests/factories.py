from factory import fuzzy, lazy_attribute
from factory.django import DjangoModelFactory

from ..models import Company, Sector


class CompanyFactory(DjangoModelFactory):

  class Meta:
    model = Company

  name = fuzzy.FuzzyText('company')
  is_customer = False
  is_partner = False
  is_supplier = False

  @lazy_attribute
  def notes(self):
    return 'notes on {}'.format(self.name)


class SectorFactory(DjangoModelFactory):

  class Meta:
    model = Sector

  name = fuzzy.FuzzyText('sector')
