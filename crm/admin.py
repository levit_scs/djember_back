from django.contrib import admin

from .models import Company, Sector, ContactMechanism


class ContactInline(admin.StackedInline):
  model = ContactMechanism


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):

  inlines = [
    ContactInline
  ]


admin.site.register(Sector)
