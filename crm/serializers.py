from rest_framework import serializers

from .models import Company, Sector, ContactMechanism, Address


class CompanySerializer(serializers.ModelSerializer):

  class Meta:
    model = Company
    fields = (
      'id',
      'name',
      'is_customer',
      'is_partner',
      'is_supplier',
      'vat',
      'notes',
      'expected_income',
      'received',
      'sector',
      'contactmechanism_set'
    )


class ContactMechanismSerializer(serializers.ModelSerializer):

  class Meta:
    model = ContactMechanism
    fields = (
      'id',
      'mechanism_type',
      'value',
      'include_in_mailing',
      'has_unsubscribed',
    )


class AddressSerializer(serializers.ModelSerializer):

  class Meta:
    model = Address
    fields = (
      'id',
      'street',
      'zip',
      'country',

      'is_invoice',
      'is_active',
    )


class SectorSerializer(serializers.ModelSerializer):

  class Meta:
    model = Sector
    fields = (
      'id',
      'name',
    )
