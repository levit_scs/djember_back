from django.db import models
from django.conf import settings
from django.utils import timezone
from django.utils.html import format_html

from simple_history.models import HistoricalRecords
from polymorphic import PolymorphicModel
from cities_light.models import Country, Region
from intranet.models import PostalCode
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from genericm2m.models import RelatedObjectsDescriptor


class Attachement(models.Model):
  name = models.CharField(max_length=255)
  file = models.FileField(upload_to='uploads')
  history = HistoricalRecords()
  content_type = models.ForeignKey(ContentType)
  object_id = models.PositiveIntegerField()
  content_object = GenericForeignKey()


class ModelWithAttachments(models.Model):
  attachements = GenericRelation(Attachement)

  class Meta:
    abstract = True


class Sector(models.Model):
  name = models.CharField(max_length=255)

  def __str__(self):
    return self.name


class Contact(PolymorphicModel, ModelWithAttachments):
  internal_contact = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='contacts',
                                       null=True, blank=True)
  is_default_contact = models.BooleanField(default=False)
  notes = models.TextField(null=True, blank=True)

  related = RelatedObjectsDescriptor()


class Segment(models.Model):
  name = models.CharField(max_length=255)

  def __str__(self):
    return self.name


class Company(Contact):
  name = models.CharField(max_length=255)
  is_customer = models.BooleanField(default=False)
  is_partner = models.BooleanField(default=False)
  is_supplier = models.BooleanField(default=False)
  sector = models.ForeignKey(Sector, null=True, blank=True)
  segments = models.ManyToManyField(Segment, blank=True)

  vat = models.CharField(max_length=30, blank=True, null=True)

  history = HistoricalRecords()

  def __str__(self):
    return self.name

  def latest_mood(self):
    moody_event = self.event_set.exclude(mood__isnull=True).order_by('-when').first()
    if moody_event:
      return moody_event.smiley_mood()
    return None

  def smiley_str(self):
    smiley = self.latest_mood()
    if smiley is not None:
      return format_html('{} {}', smiley, self.name)
    return self.name

  def expected_income(self):
    try:
     from sale.models import OrderItem
     amount = sum([x.unit_price * x.quantity for x in OrderItem.objects.exclude(
         order__state__in=['canceled', 'done']).filter(order__customer_id=self.id)])
     return '{:.2f}'.format(amount)
    except ImportError:
      return '0.0'

  def received(self):
    return '0.0'

  @property
  def invoice_address(self):
    return self.address_set.filter(is_invoice=True).filter(is_active=True).first()

  @property
  def gender(self):
    return None

  @property
  def first_name(self):
    return None

  @property
  def last_name(self):
    return None


class Person(Contact):
  belongs_to = models.ForeignKey(Company, null=True, blank=True, related_name='members')
  last_name = models.CharField(max_length=255, blank=True, null=True)
  first_name = models.CharField(max_length=255)
  user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name='person', blank=True, null=True)
  gender = models.CharField(max_length=1,
                            choices=(('f', 'Female'), ('m', 'Male'), (None, 'unknown')),
                            null=True, blank=True)

  history = HistoricalRecords()

  def __str__(self):
    if self.last_name is not None:
      return '{} {}'.format(self.first_name, self.last_name)
    return self.first_name

  @property
  def name(self):
    return self.__str__().split(',')[0]


def _belgium():
  bel = Country.objects.filter(code2__iexact='BE').first()
  if bel:
    return bel.id
  return None


class Address(models.Model):
  contact = models.ForeignKey(Contact, related_name='addresses')
  street = models.TextField()
  zip = models.ForeignKey(PostalCode, null=True, blank=True, verbose_name='City', related_name='addresses')
  region = models.ForeignKey(Region, blank=True, null=True)
  country = models.ForeignKey(Country, default=_belgium)

  is_invoice = models.BooleanField(default=True)
  is_active = models.BooleanField(default=True)

  history = HistoricalRecords()

  def __str__(self):
    return '{}, {}'.format(self.street, self.zip)


class ContactMechanism(models.Model):
  contact = models.ForeignKey(Contact)
  mechanism_type = models.CharField(max_length=50)
  value = models.CharField(max_length=255)
  include_in_mailing = models.BooleanField(default=False)
  has_unsubscribed = models.BooleanField(default=False)

  history = HistoricalRecords()

  def __str__(self):
    return '{}: {}'.format(self.type, self.value)


class Event(models.Model):
  company = models.ForeignKey(Company, blank=True, null=True)
  person = models.ForeignKey(Person, blank=True, null=True, verbose_name="who")
  when = models.DateTimeField(default=timezone.now)
  what = models.CharField(max_length=255)
  mood = models.CharField(max_length=8, default=":|", blank=True, null=True)
  summary = models.TextField(blank=True, null=True)

  history = HistoricalRecords()

  related = RelatedObjectsDescriptor()

  class Meta:
    ordering = ['-when', ]

  def short(self):
    if self.summary is not None:
      return self.summary[:30]
    return ''

  def who(self):
    if self.person is not None:
      return '(with {})'.format(self.person)
    return ''

  def smiley_mood(self):
    if self.mood:
      return format_html('<span class="text_smiley">{}</span> ', self.mood)
    return ''

  def related_objects(self):
    rv = []
    for o in self.related.all().generic_objects():
      rv.append(format_html('<a href="/{}/{}/{}">{}</a>', o._meta.app_label, o._meta.module_name, o.id, o))
    return '<br/>'.join(rv)

  def __str__(self):
    return '{}{} {}- on {} - {}'.format(
        self.smiley_mood(),
        self.what,
        self.who(),
        self.when.date(),
        self.short()
    )


class Reminder(models.Model):
  owner = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True)
  subject = models.CharField(max_length=255)
  when = models.DateTimeField()
  state = models.CharField(max_length=10, choices=[
    ('open', 'running'),
    ('dismissed', 'accepted'),
    ('canceled', 'canceled'),
  ], default='open')

  description = models.TextField(blank=True, null=True)
  contact = models.ForeignKey(Contact, blank=True, null=True)

  send_email = models.BooleanField(default=False)
  send_text = models.BooleanField(default=False)

  history = HistoricalRecords()

  class Meta:
    ordering = ('-state', 'when')
