from rest_framework import viewsets

from .serializers import (CompanySerializer, SectorSerializer,
  ContactMechanismSerializer, AddressSerializer)
from .models import Company, Sector, ContactMechanism, Address


class CompanyViewSet(viewsets.ModelViewSet):

  queryset = Company.objects.all()
  serializer_class = CompanySerializer


class ContactMechanismViewSet(viewsets.ModelViewSet):

  queryset = ContactMechanism.objects.all()
  serializer_class = ContactMechanismSerializer


class AddressViewSet(viewsets.ModelViewSet):

  queryset = Address.objects.all()
  serializer_class = AddressSerializer


class SectorViewSet(viewsets.ModelViewSet):

  queryset = Sector.objects.all()
  serializer_class = SectorSerializer
