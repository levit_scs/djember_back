# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Campaign',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(max_length=255)),
                ('slug', models.SlugField(max_length=255)),
                ('subject', models.CharField(max_length=255)),
                ('body', models.TextField()),
                ('sender_name', models.CharField(max_length=255)),
                ('sender_email', models.CharField(max_length=255)),
                ('segments', models.ManyToManyField(to='crm.Segment', blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='CampaignFilter',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('type', models.CharField(max_length=7, default='filter', choices=[('filter', 'Filter'), ('exclude', 'Exclude')])),
                ('filter', models.CharField(max_length=255)),
                ('filter_value', models.CharField(max_length=255)),
                ('campaign', models.ForeignKey(to='mailing.Campaign')),
            ],
        ),
        migrations.CreateModel(
            name='CampaignLink',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('position', models.PositiveIntegerField(default=0)),
                ('link', models.URLField()),
                ('campaign', models.ForeignKey(related_name='links', to='mailing.Campaign')),
            ],
            options={
                'ordering': ('position',),
            },
        ),
        migrations.CreateModel(
            name='Launch',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('launch_time', models.DateTimeField()),
                ('state', models.CharField(max_length=8, default='draft', choices=[('draft', 'draft'), ('ready', 'ready'), ('running', 'running'), ('done', 'done'), ('canceled', 'canceled')])),
                ('campaign', models.ForeignKey(to='mailing.Campaign')),
            ],
        ),
        migrations.CreateModel(
            name='Recipient',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('email', models.EmailField(max_length=254)),
                ('bounced', models.BooleanField(default=False)),
                ('read_for_sure', models.BooleanField(default=False)),
                ('campaign', models.ForeignKey(to='mailing.Campaign')),
                ('company', models.ForeignKey(related_name='company_mailings', blank=True, to='crm.Company', null=True)),
                ('contact', models.ForeignKey(related_name='contact_mailings', blank=True, to='crm.Contact', null=True)),
                ('launch', models.ForeignKey(to='mailing.Launch')),
            ],
        ),
        migrations.CreateModel(
            name='Template',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(max_length=255)),
                ('body', models.TextField()),
            ],
        ),
        migrations.AddField(
            model_name='campaign',
            name='template',
            field=models.ForeignKey(to='mailing.Template'),
        ),
        migrations.AlterUniqueTogether(
            name='campaignlink',
            unique_together=set([('campaign', 'position')]),
        ),
    ]
