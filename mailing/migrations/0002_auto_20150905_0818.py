# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mailing', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='campaign',
            name='segments',
            field=models.ManyToManyField(blank=True, to='crm.Segment'),
        ),
    ]
