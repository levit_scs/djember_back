from django.db import models
from django.core.urlresolvers import reverse
from django.conf import settings

from crm.models import Segment, Contact, Company


class Template(models.Model):
  name = models.CharField(max_length=255)
  body = models.TextField()

  def __str__(self):
    return self.name


class Campaign(models.Model):
  name = models.CharField(max_length=255)
  slug = models.SlugField(max_length=255)
  segments = models.ManyToManyField(Segment, blank=True)

  template = models.ForeignKey(Template)
  subject = models.CharField(max_length=255)
  body = models.TextField()
  sender_name = models.CharField(max_length=255)
  sender_email = models.CharField(max_length=255)

  def __str__(self):
    return self.name

  def get_links(self, recipient):
    rv = {
      'view_online': '{}{}'.format(settings.MAILING_ROOT, reverse('mailing_view', kwargs={'pk': recipient.pk})),
      'unsubscribe': '{}{}'.format(settings.MAILING_ROOT, reverse('unsubscribe', kwargs={'pk': recipient.pk})),
    }
    for link in self.links.all():
      rv['link_{}'.format(link.position)] = '{}{}'.format(
        settings.MAILING_ROOT,
        reverse('mailing_redirect', kwargs={'pk': recipient.pk, 'link_id': link.pk})
      )

    return rv

  def get_real_links(self, recipient):
    rv = {
      'view_online': '{}{}'.format(settings.MAILING_ROOT, reverse('mailing_view', kwargs={'pk': recipient.pk})),
      'unsubscribe': '{}{}'.format(settings.MAILING_ROOT, reverse('unsubscribe', kwargs={'pk': recipient.pk})),
    }
    for link in self.links.all():
      rv['link_{}'.format(link.position)] = link.link

    return rv


class CampaignFilter(models.Model):
  campaign = models.ForeignKey(Campaign)
  type = models.CharField(max_length=7, choices=(
    ('filter', 'Filter'),
    ('exclude', 'Exclude'),
  ), default='filter')
  filter = models.CharField(max_length=255)
  filter_value = models.CharField(max_length=255)


class CampaignLink(models.Model):
  campaign = models.ForeignKey(Campaign, related_name='links')
  position = models.PositiveIntegerField(default=0)
  link = models.URLField()

  class Meta:
    ordering = ('position',)
    unique_together = (('campaign', 'position'))

  def __str__(self):
    return '{} -> {}'.format(self.position, self.link)


class Launch(models.Model):
  campaign = models.ForeignKey(Campaign)
  launch_time = models.DateTimeField()
  state = models.CharField(max_length=8, choices=(
    ('draft', 'draft'),
    ('ready', 'ready'),
    ('running', 'running'),
    ('done', 'done'),
    ('canceled', 'canceled'),
  ), default='draft')

  def __str__(self):
    return '{} ({})'.format(self.launch_time, self.state)


class Recipient(models.Model):
  campaign = models.ForeignKey(Campaign)
  launch = models.ForeignKey(Launch)
  email = models.EmailField()
  company = models.ForeignKey(Company, blank=True, null=True, related_name='company_mailings')
  contact = models.ForeignKey(Contact, blank=True, null=True, related_name='contact_mailings')
  bounced = models.BooleanField(default=False)
  read_for_sure = models.BooleanField(default=False)
